@echo.安装并启动服务......
@echo off
@sc create ESMAM_Migrate_Service binPath= "%~dp0/ESMAM_Migrate_Service.exe"
@sc description ESMAM_Migrate_Service "ESMAM_Migrate_Service ESMAM迁移服务。"
@net start ESMAM_Migrate_Service
@sc config ESMAM_Migrate_Service start= AUTO  
@echo off
@echo.安装完毕！
@pause