# MJTopPlatform_For_Service

#### 项目介绍

 最简单的windows服务开发(C#版)。

#### 背景介绍

 C#开发windows服务，通过百度能搜索出很多方式实现，但都是比较繁琐的开发方式(具体怎么个繁琐这里略过)。
 
#### 安装教程

1. `Git`克隆源代码到本地。
2. `visual studio推进最新版本` 打开解决方案， Ctrl + H 快捷键，整个解决方案 `ESMAM_Migrate_Service` 修改成 自己定义的服务名。
3. 修改项目名称，修改项目属性中的`程序集名称`，`默认命名空间名称`。
4. 在 `ServiceUtil.cs `编写业务的逻辑代码。
5. 在 `Program.cs`进行对`ServiceUtil.cs `代码进行测试。

#### 使用说明
1. 修改`App.config`中的 `Interval`配置服务的任务执行周期
2. 修改 `Install.bat` 与 `Uninstall.bat` 属性 修改 `如果较新则复制`
3. 修改 `Program.cs` 中 的 `手动执行代码` 块，生成项目
3. 通过 `Install.bat` 以管理员运行来注册服务
4. 通过 `Uninstall.bat` 以管理员运行，停止并卸载删除服务


